# Sardi Icon Theme

HomePage: [https://github.com/erikdubois/Sardi](https://github.com/erikdubois/Sardi).

Source: [https://sourceforge.net/projects/sardi/](https://sourceforge.net/projects/sardi/)

## INSTALLATION

### From the [AUR](https://aur.archlinux.org/packages/sardi-icons/):
```
git clone https://aur.archlinux.org/sardi-icons.git
sardi-icons/
makepkg -sci
```

### From the binary package (Debian-based Linux distributions):

Download the latest sardi-icons package from [here](https://github.com/entexsoft/deb-packages/tree/master/packages).

After download you need then run this command (like root):

```
dpkg -i sardi-icons*.deb
```

### Install from source:
Unpack the source package and run command (like root):
```
make install
```

### Uninstall from source:
Run command (like root):
```
make uninstall
```

### Build and install from AUR
You need these packages for building the ArchLinux package:
```
base-devel git wget yajl
```

Run command (like normal user):
```
git clone https://aur.archlinux.org/sardi-icons.git
cd sardi-icons/
makepkg -sci
```

### Build Debian own package
You need these packages for building the Debian package:
```
build-essential make fakeroot util-linux debianutils lintian dpkg coreutils
```

Run command (like root):
```
make build-deb
```
### Install from *.deb package:
```
dpkg -i distrib/sardi-icons*.deb
```
